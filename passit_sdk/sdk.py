from django.core.urlresolvers import reverse
from django.contrib.auth.hashers import PBKDF2PasswordHasher
from django.template.defaultfilters import slugify
from simple_asym import AsymCrypt
from access.models import DEFAULT_HASH_ITERATIONS
from .exceptions import (
    PassitSDKException, PassitAPIException, PassitAuthenticationRequired)
import base64
import typing


class PassitTestClient:
    """ Simulates a client app """
    user_id = None
    public_key = None
    private_key = None

    def __init__(self, client=None):
        """ Client should be DRF test client or requests """
        if client is None:
            import requests
            client = requests
        self.client = client

    def is_username_available(self, email: str) -> bool:
        """ Check is a username (email is username) is available in back-end

        :return: Boolean True when username is available
        """
        try:
            url = reverse('username-available', args=[email])
        except:
            raise PassitSDKException('Invalid email')
        res = self.client.get(url)
        if res.status_code != 200:
            raise PassitAPIException(res, 'Unable to check username')
        return res.data.get('available')

    def sign_up(self,
                email: str,
                password: str,
                iterations=DEFAULT_HASH_ITERATIONS,
                first_name=None,
                last_name=None) -> dict:
        """ Sign up for new account

        :return: User ID """
        # Hash the password "client" side first
        hasher = PBKDF2PasswordHasher()
        client_salt = hasher.salt()
        password_hash = self._hash_password(password, client_salt)

        # Generate keys client side
        asym = AsymCrypt()
        private, public = asym.make_rsa_keys(passphrase=password)

        data = {
            "email": email,
            "password": password_hash,
            "public_key": public.decode(),
            "private_key": private.decode(),
            "client_salt": client_salt,
            # alg and iterations will just be default
        }

        url = reverse('users-list')
        res = self.client.post(url, data)
        if res.status_code != 201:
            raise PassitAPIException(res, 'Unable to check username')
        return res.data

    def log_in(self, username: str, password: str) -> dict:
        """ Log in user signing in with hashed version of password
        """
        url = reverse('user-public-auth', args=[username])
        res = self.client.get(url)
        if res.status_code != 200:
            raise PassitAPIException(res, 'Unable to get public user info')
        data = res.json()

        hashed_password = self._hash_password(
            password, data['client_salt'], data['client_iterations'])

        auth = base64.b64encode(
            username.encode() + b":" + hashed_password.encode()
        ).decode()
        self.client.credentials(HTTP_AUTHORIZATION="BASIC " + auth)
        url = reverse('knox_login')
        res = self.client.post(url)
        if res.status_code != 200:
            raise PassitAPIException(res, 'Unable to log in')

        (public, private) = self._get_own_keys(password)
        self.public_key = public
        self.private_key = private
        return res.data

    def create_group(self, name: str, slug=None) -> dict:
        """ Create a new Group

        @return: The new group's id
        """
        if slug is None:
            slug = slugify(name)
        if not self.public_key:
            raise PassitAuthenticationRequired()
        # Generate keys client side
        group_asym = AsymCrypt()
        private_key, public_key = group_asym.make_rsa_keys()

        # Generate AES key
        group_user_asym = AsymCrypt()
        group_user_asym.make_aes_key()
        aes_key = group_user_asym.get_encrypted_aes_key(
            self.public_key, use_base64=True)
        private_key_ciphertext = group_user_asym.encrypt(private_key).decode()

        url = reverse('groups-list')
        data = {
            "name": name,
            "slug": slug,
            "public_key": public_key.decode(),
            "key_ciphertext": aes_key.decode(),
            "private_key_ciphertext": private_key_ciphertext,
        }
        res = self.client.post(url, data)
        if res.status_code != 201:
            raise PassitAPIException(res, 'Couldn\'t create group')
        return res.data

    def add_user_to_group(self, group_id: int, user_id: int) -> dict:
        """ Add a new user to a group
        """
        # Get group's private key
        group_private_key = self._get_group_private_key(group_id)
        # Generate new AES key
        asym = AsymCrypt()
        asym.make_aes_key()
        # Encrypt AES key with user's public key
        invitee_public_key = self._get_user_public_key(user_id)
        encrypted_group_aes_key = asym.get_encrypted_aes_key(
            invitee_public_key, use_base64=True)
        encrypted_group_private_key = asym.encrypt(group_private_key)
        # POST data
        url = reverse('group-users-list', args=[group_id])
        data = {
            "user": user_id,
            "key_ciphertext": encrypted_group_aes_key.decode(),
            "private_key_ciphertext": encrypted_group_private_key.decode(),
        }
        res = self.client.post(url, data)
        return res.data

    def create_secret(self,
                      name: str,
                      visible_data: dict,
                      secrets: dict,
                      group_id=None) -> dict:
        """ Create a new secret

        Example of data structure

        {
            "url": "www.example.com",
            "username": "user"
        }

        :param name: Name of the new secret
        :param visible_data: key, value dict of any unencrypted data
        :param secrets: key, value dict of any encrypted data
        :param group_id: Optional param for creating a secret that is part of a
        group
        :return: Secret
        """
        url = reverse('secrets-list')
        if group_id:
            group_public_key = self._get_group_public_key(group_id)
            encrypted_key, encrypted_secrets = self._encrypt_secrets(
                group_public_key,
                secrets,
            )
        else:
            encrypted_key, encrypted_secrets = self._encrypt_secrets(
                self.public_key,
                secrets,
            )

        data = {
            "name": name,
            "type": "website",
            "data": visible_data,
            "secret_through_set": [
                {
                    "group": group_id,
                    "data": encrypted_secrets,
                    "key_ciphertext": encrypted_key,
                },
            ],
        }

        res = self.client.post(url, data, format='json')
        if res.status_code != 201:
            raise PassitAPIException(res, 'Couldn\'t create secret')
        return res.data

    def update_secret(self,
                      secret_id: int,
                      name=None,
                      visible_data=None,
                      secret_data=None,
                      group_id=None) -> dict:
        """ """
        url = reverse('secrets-detail', args=[secret_id])
        if secret_data is None:  # Just patch then
            patch_data = {}
            if name:
                patch_data['name'] = name
            if visible_data:
                patch_data['data'] = visible_data
            res = self.client.patch(url, patch_data, format='json')
            if res.status_code != 200:
                raise PassitAPIException(res, "Could't patch secret")
            return res.data

        secret = self.get_secret(secret_id)
        for secret_through in secret['secret_through_set']:
            encrypted_key, encrypted_secrets = self._encrypt_secrets(
                secret_through['public_key'],
                secret_data,
            )
            secret_through['data'] = encrypted_secrets
            secret_through['key_ciphertext'] = encrypted_key

        data = {
            "name": name,
            "type": "website",
            "data": visible_data,
            "secret_through_set": secret['secret_through_set'],
        }

        res = self.client.put(url, data, format='json')
        if res.status_code != 200:
            raise PassitAPIException(res, 'Couldn\'t update secret')
        return res.data

    def get_secret(self, secret_id: int) -> dict:
        """ Get a secret. Does not decrypt data
        """
        url = reverse('secrets-detail', args=[secret_id])
        res = self.client.get(url)
        if res.status_code != 200:
            raise PassitAPIException(res, 'Couldn\'t get secret')
        return res.data

    def list_secrets(self) -> list:
        """ Get list of secrets. Does not decrypt data """
        url = reverse('secrets-list')
        res = self.client.get(url)
        if res.status_code != 200:
            raise PassitAPIException(res, 'Couldn\'t get secrets')
        return res.data

    def decrypt_secret(self, secret: dict) -> dict:
        """
        Decrypts ciphertext from a secret.

        :param secret: Unique secret object as returned from api
        :return: Plaintext of encrypted data in key, value format
        """
        # Any secret should work, get the first one.
        for secret_through in secret.get('secret_through_set'):
            if secret_through['is_mine']:
                if secret_through.get('group'):
                    private_key = self._get_group_private_key(secret_through['group'])
                else:
                    private_key = self.private_key
                break

        secrets = self._decrypt_secrets(
            private_key,
            secret_through.get('key_ciphertext'),
            secret_through.get('data'),
        )
        return secrets

    def add_group_to_secret(self,
                            group_id: int,
                            secret: typing.Union[int, dict]) -> dict:
        """ Add group to secret

        :param group_id: Group we want to add secret to
        :param secret: Either secret id or the secret object itself (for optimization)
        :return: SecretThrough response object
        """
        if isinstance(secret, int):
            secret = self.get_secret(secret)

        secret_data = self.decrypt_secret(secret)

        group_public_key = self._get_group_public_key(group_id)
        encrypted_key, encrypted_secrets = self._encrypt_secrets(
            group_public_key,
            secret_data,
        )

        url = reverse('secret-groups-list', kwargs={'id': secret['id']})
        data = {
            "group": group_id,
            "key_ciphertext": encrypted_key,
            "data": encrypted_secrets,
        }

        res = self.client.post(url, data, format='json')
        if res.status_code != 201:
            raise PassitAPIException(res, 'Unable to add group to secret')
        return res.data

    @staticmethod
    def _hash_password(password: str,
                       salt=None,
                       iterations=DEFAULT_HASH_ITERATIONS) -> str:
        """ Hash a password with salt, alg, and number of iterations

        :rtype: hashed password """
        hasher = PBKDF2PasswordHasher()
        if salt is None:
            salt = hasher.salt()
        return hasher.encode(password, salt, iterations)

    def _get_user_public_key(self, user_id: int) -> str:
        """ Get another user's public key

        :return: Public Key
        """
        url = reverse('user-public-key', args=[user_id])
        res = self.client.get(url)
        if res.status_code != 200:
            raise PassitAPIException(res, 'Unable to get user public key')
        return res.data.get('public_key')

    def _get_own_keys(self, password: str) -> typing.Tuple[str, typing.Any]:
        """ Get and decrypt own RSA keys.

        :return: (public_key, private_key)"""
        url = reverse('users-list')
        res = self.client.get(url)
        if res.status_code != 200:
            raise PassitAPIException(res, 'Unable to get private user info')
        public = res.data[0]['public_key']
        private = res.data[0]['private_key']

        asym = AsymCrypt()
        asym.set_private_key(private, password)

        return public, asym.private_key

    def _get_group_public_key(self, group_id: int) -> str:
        """ Return public key for a group """
        url = reverse('groups-detail', args=[group_id])
        res = self.client.get(url)
        if res.status_code != 200:
            raise PassitAPIException(res, 'Unable to get group public key')
        return res.data['public_key']

    def _get_group_private_key(self, group_id: int) -> bytes:
        """ Mock client gets the group private key

        :return: Private RSA Key
        """
        url = reverse('groups-detail', args=[group_id])
        res = self.client.get(url)

        group_aes = res.data['my_key_ciphertext']
        group_private_key = res.data['my_private_key_ciphertext']

        asym = AsymCrypt()
        asym.set_private_key(self.private_key)
        asym.set_aes_key_from_encrypted(group_aes, use_base64=True)
        return asym.decrypt(group_private_key.encode())

    @staticmethod
    def _decrypt_secrets(private_key: typing.Any,
                         encrypted_key: str,
                         encrypted_secrets: dict) -> dict:
        """ Decrypt a group secret.

        :return: Dict of plaintext secrets
        """
        asym = AsymCrypt()
        asym.set_private_key(private_key)
        asym.set_aes_key_from_encrypted(encrypted_key, use_base64=True)
        secrets = {}
        for name, value in encrypted_secrets.items():
            secrets[name] = asym.decrypt(value.encode()).decode()
        return secrets

    @staticmethod
    def _encrypt_secrets(public_key: str, secrets: dict) -> typing.Tuple[str, dict]:
        """ Encrypt a dict of secrets using a public key

        :return: tuple (Encrypted AES key, encrypted secrets)
        """
        asym = AsymCrypt(public_key=public_key)
        key = asym.make_aes_key()
        encrypted_key = asym.rsa_encrypt(key, use_base64=True).decode()

        encrypted_secrets = {}
        for name, value in secrets.items():
            encrypted_secrets[name] = asym.encrypt(value).decode()
        return encrypted_key, encrypted_secrets
