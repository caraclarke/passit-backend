from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import routers
from access.views import (
    UserPublicAuthView, UserViewSet, GroupViewSet, GroupUserViewSet,
    UsernameAvailableView, UserLookUpView, UserPublicKeyView)
from secrets.views import SecretViewSet, SecretThroughViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet, base_name='users')
router.register(r'groups', GroupViewSet, base_name='groups')
router.register(r'groups/(?P<id>\d+)/users', GroupUserViewSet, base_name='group-users')
router.register(r'secrets', SecretViewSet, base_name='secrets')
router.register(r'secrets/(?P<id>\d+)/groups', SecretThroughViewSet, base_name='secret-groups')

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(router.urls)),
    url(r'^api/user-public-auth/(?P<email>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})', UserPublicAuthView.as_view(), name='user-public-auth'),
    url(r'^api/username-available/(?P<email>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})', UsernameAvailableView.as_view(), name='username-available'),
    url(r'^api/user-lookup/(?P<email>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})', UserLookUpView.as_view(), name='user-available'),
    url(r'^api/user-public-key/(?P<id>\d+)/$', UserPublicKeyView.as_view(), name='user-public-key'),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/auth/', include('knox.urls')),
    url(r'^docs/', include('rest_framework_swagger.urls')),
]
