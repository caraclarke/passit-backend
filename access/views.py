from django.shortcuts import get_object_or_404
from rest_framework import viewsets, permissions, generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.throttling import UserRateThrottle, AnonRateThrottle
from .serializers import (
    UserSerializer, GroupSerializer, DetailedGroupSerializer,
    GroupUserSerializer, GroupUserUpdateSerializer,
    UserPublicAuthSerializer, UsernameAvailableSerializer,
    PublicUserSerializer, UserPublicKeySerializer
)
from .models import GroupUser, Group, User


class Burst10MinUser(UserRateThrottle):
    rate = '10/minute'
    scope = 'burst'


class Sustained200DayUser(UserRateThrottle):
    rate = '200/day'
    scope = 'sustained'


class Burst10MinAnon(AnonRateThrottle):
    rate = '10/minute'
    scope = 'burst'


class Sustained200DayAnon(AnonRateThrottle):
    rate = '200/day'
    scope = 'sustained'


class UserPublicAuthView(generics.RetrieveAPIView):
    """ Request a users publically known client side hashing
    algorithym, iteractions, and salt. No authentication required. """
    permission_classes = [permissions.AllowAny]
    queryset = User.objects.all()
    serializer_class = UserPublicAuthSerializer
    lookup_field = 'email'
    throttle_classes = (Burst10MinUser, Sustained200DayUser, Burst10MinAnon,
						Sustained200DayAnon)


class UserLookUpView(generics.RetrieveAPIView):
    """ Look up a users id, firstname and lastname by an email """
    permission_classes = [permissions.IsAuthenticated]
    queryset = User.objects.all()
    serializer_class = PublicUserSerializer
    lookup_field = 'email'
    throttle_classes = (Burst10MinUser, Sustained200DayUser, Burst10MinAnon,
                                            Sustained200DayAnon)


class UserPublicKeyView(generics.RetrieveAPIView):
    permission_classes = [permissions.IsAuthenticated]
    queryset = User.objects.all()
    serializer_class = UserPublicKeySerializer
    lookup_field = 'id'


class UsernameAvailableView(APIView):
    """ Check if username is available for new user.

    Accepts GET requests like `/api/username-available/email@example.com/`

    If username is available returns `{"available": true}`
    """
    permission_classes = [permissions.AllowAny]
    throttle_classes = (Burst10MinUser, Sustained200DayUser, Burst10MinAnon,
                        Sustained200DayAnon)
    serializer_class = UsernameAvailableSerializer

    def get(self, request, format=None, **kwargs):
        serializer = self.serializer_class(data=kwargs)
        if serializer.is_valid():
            email = serializer.data.get('email')
            if User.objects.filter(email=email).exists():
                return Response({"available": False})
            return Response({"available": True})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserViewSet(viewsets.ModelViewSet):
    """ Create and get users.

    To create:

    1. Generate new RSA keys client side. Encrypt private key using
    user entered password and make_longer_pass function (Note this function
    will be rewritten before 1.0).
    2. (TODO) hash the password
    3. Send hashed password and encrypted private key.
    """
    serializer_class = UserSerializer
    permission_classes = [permissions.AllowAny]  # For making new users

    def get_queryset(self):
        """ Return only current user or none """
        user = self.request.user
        if user.is_authenticated():
            return [user]
        return []


class IsGroupAdminOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.groupuser_set.filter(
            user=request.user, is_group_admin=True,
        ).exists()


class GroupViewSet(viewsets.ModelViewSet):
    """ A group is what gives users permission to view secrets
    The detail view (/groups/<id>) will give some read only data related to
    users in this group.

    If you need to edit group memebership and admin status see
    Group Users `/api/groups/<group_id>/users/`

    Creating a new group will automatically assign the current user as an
    group admin member. As always encrypt data client side.

    ### How to create a new Group:

    1. Ensure you have your personal rsa keys
    2. Generate new RSA keys for group
    3. Generate new AES key for group
    4. Encrypt AES key using your own public RSA key
    5. Encrypt group RSA key using AES key
    6. Submit example

            {
                "name": "My Group",
                "public_key": "-----BEGIN PUBLIC KEY....",
                "key_ciphertext": "a3b7fff3...",
                "private_key_ciphertext": "a2112d5...",
            }
    """
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated, IsGroupAdminOrReadOnly]

    def get_queryset(self):
        return self.request.user.groups.all()

    def get_serializer_class(self):
        # Give a more detailed result for just one object
        if self.action == 'retrieve':
            return DetailedGroupSerializer
        return super().get_serializer_class()


class GroupUserPermission(permissions.BasePermission):
    def check_group_admin(self, user, group):
        """ Returns True if the user is an admin for the group """
        return group.groupuser_set.filter(
            user=user, is_group_admin=True,
        ).exists()

    def check_group_membership(self, user, group):
        """ Returns True if the user is in group """
        return group.groupuser_set.filter(
            user=user
        ).exists()

    def basic_user_checks(self, user):
        """ Inactive users can do nothing Staff users can do anything.
        This overides other checks.
        Return None if there is no override
        """
        if user.is_active is False:
            return False
        if user.is_staff is True:
            return True

    def has_object_permission(self, request, view, obj):
        perm = self.basic_user_checks(request.user)
        if perm is not None:
            return perm

        if request.method in permissions.SAFE_METHODS:
            return self.check_group_membership(request.user, obj.group)
        return self.check_group_admin(request.user, obj.group)

    def has_permission(self, request, view):
        group = view.get_group()

        if request.method in permissions.SAFE_METHODS:
            return self.check_group_membership(request.user, group)

        return self.check_group_admin(request.user, group)


class GroupUserViewSet(viewsets.ModelViewSet):
    """ Users in a group. Allows editing if current user is a group admin.

    Does not allow editing group or user when updating. For this
    please delete the object and make a new one.

    ## How to add a user

    1. First get the user's public key and the group's unencrypted private key
    (you must decrypt this client side).
    2. Generate a new AES key.
    3. Encrypt the AES key using the user's public key
    4. Encrypt the group private key using the AES key
    5. Submit everything in it's encrypted form. Never post plain text secrets
    to the server (Assume the server is evil and hates you at all times)
    """
    serializer_class = GroupUserSerializer
    permission_classes = [GroupUserPermission]

    def get_serializer_class(self):
        if self.request.method in ['PUT', 'PATCH']:
            return GroupUserUpdateSerializer
        return super().get_serializer_class()

    def get_group(self):
        return get_object_or_404(Group, pk=self.kwargs.get('id'))

    def get_queryset(self):
        group = self.get_group()
        user = self.request.user
        return GroupUser.objects.filter(
            group=group,
            group__users=user,
        )

    def perform_create(self, serializer):
        group = self.get_group()
        serializer.save(group=group)

    def perform_update(self, serializer):
        group = self.get_group()
        serializer.save(group=group)
