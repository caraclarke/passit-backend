from django.core.urlresolvers import reverse
from rest_framework.test import APITestCase, APIClient
from access.models import User, Group
from .models import Secret
from passit.test_client import PassitTestMixin
from passit_sdk import PassitTestClient


TEST_ADMIN_PASSWORD = 'aaaaaaaa'
TEST_USER_PASSWORD = 'bbbbbbbb'
USER2_PASS = 'cccccccc'


class TestSecretAPI(PassitTestMixin, APITestCase):
    fixtures = ['users.json']

    def test_secrets(self):
        """ Create and get a secret for one user. """
        user = User.objects.filter(is_staff=False).first()
        self.sdk.log_in(user.email, TEST_USER_PASSWORD)
        secrets = {"password": "123456"}
        visible_data = {"url": "www.example.com"}

        secret = self.sdk.create_secret("test secret", visible_data, secrets)
        secret_id = secret.get('id')

        # Ensure we can get it from the sdk
        self.assertEqual(
            secret,
            self.sdk.get_secret(secret_id)
        )

        # It should also show in a list from the sdk
        secrets_list = self.sdk.list_secrets()
        self.assertEqual(
            secret['name'],
            secrets_list[0]['name']
        )

        # Ensure we can decrypt secret data
        decrypted_secrets = self.sdk.decrypt_secret(secret)
        self.assertEqual(decrypted_secrets, secrets)

    def test_secrets_errors(self):
        user = User.objects.filter(is_staff=False).first()
        self.sdk.log_in(user.email, TEST_USER_PASSWORD)
        secrets = {"password": "123456"}
        # data = {"url": "www.example.com"}

        try:
            self.sdk.create_secret("test secret", 'lol', secrets)
        except Exception as e:
            self.assertEqual(e.res.status_code, 400)

    def test_patch_secret(self):
        user = User.objects.filter(is_staff=False).first()
        self.sdk.log_in(user.email, TEST_USER_PASSWORD)
        secrets = {"password": "123456"}
        visible_data = {"url": "www.example.com"}
        secret = self.sdk.create_secret("test secret", visible_data, secrets)

        secret = self.sdk.update_secret(secret.get('id'), name="patched!")
        self.assertEqual(
            Secret.objects.get(pk=secret.get('id')).name,
            "patched!"
        )



class TestGroupSecretAPI(PassitTestMixin, APITestCase):
    fixtures = ['users.json', 'groups.json']

    def test_group_secret(self):
        """ Test creating a new secret, shared with a group """
        user = User.objects.get(email="noadmin@aa.aa")
        self.sdk.log_in(user.email, TEST_USER_PASSWORD)
        group = user.groups.get(name="accounting")
        visible_data = {"url": "www.example.com"}
        secrets = {"password": "123456"}

        secret = self.sdk.create_secret(
            "My site", visible_data, secrets, group.id)
        secret_id = secret.get('id')

        self.assertEqual(
            secret,
            self.sdk.get_secret(secret_id)
        )

        # Ensure we can decrypt secret data
        decrypted_secrets = self.sdk.decrypt_secret(secret)
        self.assertEqual(decrypted_secrets, secrets)

    def test_add_secret_to_existing_group(self):
        """ Create a group and then add a secret to it """
        user1 = User.objects.get(email="noadmin@aa.aa")
        group1 = user1.groups.get(name="accounting")
        self.sdk.log_in(user1.email, TEST_USER_PASSWORD)

        # Create a secret and a second group
        visible_data = {"url": "www.example.com"}
        secret_data = {"password": "123456"}
        secret = self.sdk.create_secret(
            "my site", visible_data, secret_data, group1.id)
        group2 = self.sdk.create_group("group2", 'group2')

        self.assertEquals(
            Secret.objects.get(pk=secret['id']).secret_through_set.count(),
            1
        )

        self.sdk.add_group_to_secret(group2['id'], secret)

        self.assertEquals(
            Secret.objects.get(pk=secret['id']).secret_through_set.count(),
            2
        )

    def test_update_secret(self):
        user = User.objects.get(email="noadmin@aa.aa")
        self.sdk.log_in(user.email, TEST_USER_PASSWORD)

        # Create a secret and a second group
        visible_data = {"url": "www.example.com"}
        secret_data = {"password": "123456"}
        secret = self.sdk.create_secret(
            "my site", visible_data, secret_data)
        self.assertEqual(
            self.sdk.decrypt_secret(secret),
            secret_data
        )
        initial_key = secret['secret_through_set'][0]['key_ciphertext']

        new_secret_data = {"password": "abcdef"}
        secret = self.sdk.update_secret(
            secret['id'], "my site edited", visible_data, new_secret_data)
        self.assertEqual(
            secret,
            self.sdk.get_secret(secret['id']),
            msg="The returned updated secret should match a new copy of it"
        )
        self.assertNotEqual(
            initial_key,
            secret['secret_through_set'][0]['key_ciphertext'],
            msg="The AES key should have changed"
        )
        self.assertEqual(
            self.sdk.decrypt_secret(secret),
            new_secret_data
        )

    def test_two_users_one_group_secret(self):
        user1 = User.objects.get(email="noadmin@aa.aa")
        user1_sdk = self.sdk
        user1_sdk.log_in(user1.email, TEST_USER_PASSWORD)
        user2 = User.objects.get(email="another@example.com")
        user2_sdk = PassitTestClient(client=APIClient())
        user2_sdk.log_in(user2.email, USER2_PASS)
        group = user1.groups.get(name="accounting")

        visible_data = {"url": "www.example.com"}
        secrets = {"password": "123456"}

        # store a secret
        secret = user1_sdk.create_secret(
            "my site", visible_data, secrets, group.id)

        # Invite user2 to our group
        # Make AES and RSA keys
        user1_sdk.add_user_to_group(group.id, user2.id)
        self.assertEqual(group.users.count(), 2)

        # User2 can get the password
        secret_user2 = user2_sdk.get_secret(secret['id'])
        self.assertEqual(
            user2_sdk.decrypt_secret(secret_user2),
            secrets
        )

        # User2 can change the secret
        secrets = {"password": "hunter2"}
        user2_sdk.update_secret(
            secret_user2['id'], "my site edited", visible_data, secrets)
        self.assertEqual(
            Secret.objects.get(pk=secret_user2['id']).name,
            "my site edited",
        )
        secret_user2 = user2_sdk.get_secret(secret['id'])
        self.assertEqual(
            user2_sdk.decrypt_secret(secret_user2),
            secrets
        )

        # User1 can get the password
        secret_user1 = user1_sdk.get_secret(secret['id'])
        self.assertEqual(
            user1_sdk.decrypt_secret(secret_user1),
            secrets
        )

    def test_two_groups_one_secret(self):
        """ In this test we have 3 users, each in 2 groups
        sharing 1 secret. When a user changes the secret the other users
        should be able to view it even though neither group contains both users.

        Sally shares a secret with David. (group1)
        David shares the secret with Amy. (group2)
        David is in both groups.
        Sally and Amy don't know each other and are not in the same group.
        Amy is able to change the secret and Sally is able to view the change.
        """
        sally = User.objects.get(email="noadmin@aa.aa")
        sally_sdk = self.sdk
        sally_sdk.log_in(sally.email, TEST_USER_PASSWORD)
        david = User.objects.get(email="another@example.com")
        david_sdk = PassitTestClient(client=APIClient())
        david_sdk.log_in(david.email, USER2_PASS)
        amy = User.objects.get(email="aa@aa.aa")
        amy_sdk = PassitTestClient(client=APIClient())
        amy_sdk.log_in(amy.email, TEST_ADMIN_PASSWORD)

        # Make group1
        group1 = sally_sdk.create_group('group1', 'group1')
        sally_sdk.add_user_to_group(group1['id'], david.id)

        # Make a secret shared with group1
        secrets = {"password": "123456"}
        visible_data = {"url": "www.example.com"}
        unique_secret = sally_sdk.create_secret(
            "my site", visible_data, secrets, group1['id'])

        # Make group2 and add amy
        group2 = david_sdk.create_group('group2', 'group2')
        david_sdk.add_user_to_group(group2['id'], amy.id)

        # David adds secret to group2
        david_sdk.add_group_to_secret(group2['id'], unique_secret)

        # Amy can read secret
        secret_amy = amy_sdk.get_secret(unique_secret['id'])
        self.assertEqual(
            amy_sdk.decrypt_secret(secret_amy),
            secrets
        )

        # Amy can change secret
        secrets = {"password": "hunter2"}
        amy_sdk.update_secret(
            secret_amy['id'], "my site edited", visible_data, secrets)
        self.assertEqual(
            Secret.objects.get(pk=secret_amy['id']).name,
            "my site edited",
        )
        self.assertEqual(
            Secret.objects.get(pk=secret_amy['id']).secret_through_set.count(),
            2
        )

        # Sally can read secret and it's now what Amy set it as
        secret_sally = sally_sdk.get_secret(unique_secret['id'])
        self.assertEqual(
            sally_sdk.decrypt_secret(secret_sally),
            secrets
        )
