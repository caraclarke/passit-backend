from django.db import models
from django.core.mail import send_mail
from django.core.validators import (
    RegexValidator, MinLengthValidator, MinValueValidator)
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from simple_asym import AsymCrypt


DEFAULT_HASH_ITERATIONS = 24000


class PublicKeyField(models.TextField):
    """ Public Key field includes some validation to ensure the data is actually
    a public key saved in the correct format
    """
    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 900
        kwargs['validators'] = [
            RegexValidator(
                regex='^-----BEGIN (RSA )?PUBLIC KEY-----.*',
                message='Not a public key',
            ),
            RegexValidator(
                regex='.*-----END (RSA )?PUBLIC KEY-----$',
                message='Not a public key',
            ),
            MinLengthValidator(400),
        ]
        super().__init__(*args, **kwargs)


class CiphertextField(models.TextField):
    """ Attempts to validate the data is actually ciphertext with a few
    sanity checks."""
    def __init__(self, *args, **kwargs):
        kwargs['validators'] = [
            MinLengthValidator(400),
        ]
        super().__init__(*args, **kwargs)


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, private_key, public_key,
                     **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)

        if private_key is None and public_key is None:
            asym = AsymCrypt()
            private_key, public_key = asym.make_rsa_keys(passphrase=password)

        user = self.model(email=email,
                          private_key=private_key,
                          public_key=public_key,
                          **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password, private_key=None, public_key=None,
                    **extra_fields):
        extra_fields.setdefault('is_staff', False)
        return self._create_user(email, password, private_key, public_key,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractBaseUser):
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    email = models.EmailField(_('email address'), unique=True)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    # Passwords are hashed both server side and client side.
    client_alg = models.CharField(max_length=20, default='SHA256')
    client_iterations = models.PositiveIntegerField(
        default=DEFAULT_HASH_ITERATIONS,
        validators=[
            MinValueValidator(10000),
        ]
    )
    client_salt = models.CharField(
        max_length=512,
        validators=[
            MinLengthValidator(12),
        ]
    )
    public_key = PublicKeyField()
    private_key = models.TextField(
        max_length=8000,
        validators=[
            RegexValidator(
                regex='^-----BEGIN ENCRYPTED PRIVATE KEY-----.*',
                message='Not a private key',
            ),
            RegexValidator(
                regex='.*-----END ENCRYPTED PRIVATE KEY-----$',
                message='Not a private key',
            ),
            MinLengthValidator(1500),
        ],
        help_text="Must be encrypted using passphrase"
    )
    organizations = models.ManyToManyField('Organization', blank=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def has_module_perms(self, app_label):
        return True

    def has_perm(self, perm, obj=None):
        return True

    def __str__(self):
        return self.email


class Organization(models.Model):
    name = models.CharField(max_length=1000)

    def __str__(self):
        return self.name


class GroupManager(models.Manager):
    def create(self, user, name, slug, public_key, key_ciphertext,
               private_key_ciphertext, **extra_fields):
        """ Set up a group with a access group user (required) """
        group = self.model(name=name, slug=slug, public_key=public_key, **extra_fields)
        group.save()

        GroupUser.objects.create(
            user=user,
            group=group,
            is_group_admin=True,
            key_ciphertext=key_ciphertext,
            private_key_ciphertext=private_key_ciphertext,
        )
        return group


class Group(models.Model):
    name = models.CharField(max_length=1000)
    slug = models.SlugField(max_length=1000, default="default-slug")
    organization = models.ForeignKey(Organization, blank=True, null=True)
    users = models.ManyToManyField(
        User, through='access.GroupUser', blank=True, related_name="groups")
    public_key = PublicKeyField()

    objects = GroupManager()

    class Meta:
        verbose_name = "Group"
        unique_together = ('name', 'organization')

    def __str__(self):
        return self.name

    def get_keys(self, user):
        group_user = self.groupuser.get(user=user)
        aes = group_user.key_ciphertext
        private = group_user.encrypted_private_key
        return aes, private

    def get_my_key_ciphertext(self, user):
        group_user = self.groupuser_set.get(user=user)
        return group_user.key_ciphertext

    def get_my_private_key_ciphertext(self, user):
        group_user = self.groupuser_set.get(user=user)
        return group_user.private_key_ciphertext


class GroupUser(models.Model):
    user = models.ForeignKey(User)
    group = models.ForeignKey(Group)
    is_group_admin = models.BooleanField(
        default=False, help_text="Is able to add/remove members")
    key_ciphertext = CiphertextField()
    private_key_ciphertext = CiphertextField()

    class Meta:
        unique_together = ('user', 'group')

    def __str__(self):
        return '{} - {}'.format(self.user, self.group)
