from django.shortcuts import get_object_or_404
from rest_framework import viewsets, mixins
from .serializers import SecretSerializer, SecretThroughSerializer
from .models import Secret, SecretThrough


class SecretViewSet(viewsets.ModelViewSet):
    """ Manage personal and group secrets.

    Secrets must be stored in a key value json structure like

        `{"password": "hunter2", "security answer": "The Moon"}`
    Note that nested json is not supported.

    ## Create or update a personal secret

    1. Ensure you have your own public key.
    2. Generate a new AES key. Do not reuse AES keys.
    3. Encrypt the new AES key with your public key.
    4. Encrypt each value in the key value structure. Example:

        `{"password": "cyphertext1", "security answer": "cyphertext2"}`

    5. Submit everything. Use POST for a new secret. Use PUT to update existing
    Example:

            {
                "name": "My site",
                "type": "website",
                "data": {"url": "www.example.com"},
                "secret_through_set": [
                    {
                        "hidden_data": {
                            "password": "cyphertext1",
                            "security answer": "cyphertext2"
                        },
                        "encrypted_key": "aes_key_ciphertext"
                    }
                ]
            }

    ## Create or update group secrets
    1. Get group(s) public key - using groups api.
    2. Generate a new AES key. Do not reuse AES keys.
    3. Encrypt the new AES key with the group's public keys.
    Do this for each group.
    4. Encrypt each value in the key value structure. See personal secret for
    example. Also do this for each group. If the secret is shared with five
    groups then you should have five different AES keys and five sets of secret
    ciphertext.
    5. Submit everything. Example:

            {
                "name": "My site",
                "type": "website",
                "data": {"url": "www.example.com"},
                "secret_through_set": [
                    {
                        "hidden_data": {
                            "password": "cyphertext1",
                            "security answer": "cyphertext2"
                        },
                        "encrypted_key": "aes_key_ciphertext",
                        "group": "3"
                    },
                    {
                        "hidden_data": {
                            "password": "cyphertext3",
                            "security answer": "cyphertext4"
                        },
                        "encrypted_key": "different_aes_key_ciphertext",
                        "group": "4"
                    }
                ]
            }
    """
    queryset = Secret.objects.all()
    serializer_class = SecretSerializer

    def get_queryset(self):
        return Secret.objects.mine(self.request.user).distinct()

    def get_serializer(self, *args, **kwargs):
        serializer = super().get_serializer(*args, **kwargs)
        if hasattr(serializer, 'fields'):
            serializer.fields['secret_through_set'] = SecretThroughSerializer(
                many=True, context=serializer.context)
        return serializer


class SecretThroughViewSet(mixins.CreateModelMixin,
                           mixins.ListModelMixin,
                           mixins.RetrieveModelMixin,
                           viewsets.GenericViewSet):
    """ Only used to create new secrets. Allows for adding a new group to an
    existing secret without changing all secrets.
    """
    serializer_class = SecretThroughSerializer

    def get_queryset(self):
        my_secrets = Secret.objects.mine(self.request.user)
        secret_id = self.kwargs.get('id')
        queryset = SecretThrough.objects.filter(secret__in=my_secrets)
        queryset = queryset.filter(secret_id=secret_id)
        return queryset

    def perform_create(self, serializer):
        secret = get_object_or_404(
            Secret, pk=self.kwargs.get('id'))
        serializer.save(secret=secret)
