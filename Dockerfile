FROM python:3.5
ENV PYTHONUNBUFFERED 1

RUN mkdir /code
WORKDIR /code

# Upgrade OpenSSL
ADD debs /debs
RUN dpkg -i /debs/*.deb

RUN mkdir /code/requirements
ADD requirements/requirements.txt /code/requirements/
RUN pip install -r requirements/requirements.txt
ADD requirements/dev-requirements.txt /code/requirements/
RUN pip install -r requirements/dev-requirements.txt

ADD . /code/
