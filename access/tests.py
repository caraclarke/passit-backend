from django.core.urlresolvers import reverse
from rest_framework.test import APITestCase
from model_mommy import mommy
from passit.test_client import PassitTestMixin
from passit_sdk.exceptions import (
    PassitAPIException, PassitAuthenticationRequired)
from .models import User, Group, GroupUser


TEST_USER_USERNAME = 'noadmin@aa.aa'
TEST_USER_PASSWORD = 'bbbbbbbb'


class TestAuthAPI(PassitTestMixin, APITestCase):
    fixtures = ['users.json']

    def test_username_available(self):
        result = self.sdk.is_username_available('aa@aa.aa')
        self.assertFalse(result)

        try:
            result = self.sdk.is_username_available('a' * 90000 + '@woo.com')
        except Exception as e:
            self.assertEqual(e.res.status_code, 400)

        result = self.sdk.is_username_available('jhgyujh@fdalfdsf.fdsa')
        self.assertTrue(result)

    def test_new_user(self):
        email = "testy@test.test"
        password = "12345678"

        self.sdk.sign_up(email, password)
        self.assertTrue(User.objects.get(email=email))

        with self.assertRaises(PassitAPIException):
            self.sdk.sign_up('nope', password)

    def test_new_user_fail(self):
        ''' Simulates an impossible state for the SDK to give, proves
        server side validation '''
        url = reverse('users-list')
        data = {
            "email": "testy@test.test",
            "password": "12345678",
            "private_key": "lol",
            "public_key": "not real",
        }
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 400)

        # Valid pub key but no private.
        data = {
            "email": "testy@test.test",
            "password": "12345678",
            "public_key": "-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA5LKLLSzMNOhG01xc1gUr\nDSX19MKL+h0u0p9bFRZ+2KOrDG3LIQSGOYCXBVsZS9eeh3OwOHFcBi+3J55BL9rZ\n32l3E7sv75H6tOMHJzRQqAj8LtayEoDyq+k5FLeZliXt980rzwFIFZ0mARzg44xS\n+Rt3HziXp+COQiqKJob40hP2TfFDy2glU4uUCBVhTHLNogDc/dECrTp+76+2vYs0\nwDL86apjuUg4iQxN1u8cddOVA/I9HC1TkX3B0Iv1jwMRRZWB73BvvuxfksbtVV+X\nnyTIvZMrlep6bhupmN4gZacQsi8bWf19S7CdQ6AC6h9ghTg9G9Duci1mlPy6FleT\nAKNiXd1Vmk9mwRcTD81uXQPM9VsNqnNa3WoQHmDXrvALE/Pv++4LaujJEr3yYNXy\nsiHMSbTeh/0Gvt9CwGLgIoQpqhrMy4y5iIngfLQeKK6p6k785zpmRVgQyxtLrmiQ\nT9xz+cfAcC5Y6ZRs9i+SweC+tH4iN1v1DZMhDM1J8B/6Nj9pp64cb/tiYTw1JIyc\n2c2gW4ovZOyAZisVf9cAJWekwLpoxZdfNuQRwoU4Z1d7G+1bOtOHXJyJBGA3QmJc\nopp823HDGXqwDcBH/6lUic6yiyjCZJF7fyhgQxAuB5zHbBA1dFp2ZBMXoahEpTdB\nk22oA9jbRF3fIgCs8nqnSL0CAwEAAQ==\n-----END PUBLIC KEY-----",
        }
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 400)

    def test_login(self):
        login = self.sdk.log_in(TEST_USER_USERNAME, TEST_USER_PASSWORD)
        self.assertEqual(
            login['user']['id'],
            User.objects.get(email=TEST_USER_USERNAME).id
        )

        with self.assertRaises(PassitAPIException):
            self.sdk.log_in(TEST_USER_USERNAME, 'nope')


class TestGroupSDK(PassitTestMixin, APITestCase):
    fixtures = ['users.json']

    def test_add_user_to_group(self):
        group_name = "My Group"
        slug = "my-group"
        invite_user = User.objects.filter(is_staff=False).last()
        self.sdk.log_in(TEST_USER_USERNAME, TEST_USER_PASSWORD)
        group_id = self.sdk.create_group(group_name).get('id')
        group = Group.objects.get(pk=group_id)
        self.assertEqual(group.slug, slug)
        self.assertEqual(group.users.count(), 1)
        self.sdk.add_user_to_group(group_id, invite_user.id)
        self.assertEqual(group.users.count(), 2)


class TestGroupAPI(PassitTestMixin, APITestCase):
    fixtures = ['users.json']

    def test_group(self):
        group_name = "My Group"
        slug = "my-group"
        url = reverse('groups-list')
        user = User.objects.filter(is_staff=False).first()

        with self.assertRaises(PassitAuthenticationRequired):
            self.sdk.create_group(group_name, slug)

        self.sdk.log_in(TEST_USER_USERNAME, TEST_USER_PASSWORD)

        group_id = self.sdk.create_group(group_name, slug).get('id')

        group = Group.objects.get(pk=group_id)
        self.assertEqual(group.name, group_name)

        group_user = group.groupuser_set.get(user=user)
        self.assertEqual(group.name, group_name)
        self.assertEqual(group_user.is_group_admin, True)

        # GET
        res = self.client.get(url)
        self.assertContains(res, group_name)

        # user2 can't see user1's groups
        user2 = User.objects.filter(is_staff=False).exclude(pk=user.pk).first()
        self.client.force_authenticate(user=user2)
        res = self.client.get(url)
        self.assertNotContains(res, group_name)

        # PATCH existing group should be allowed
        self.client.force_authenticate(user=user)
        data = {
            "name": "My Edited Group"
        }
        url = reverse('groups-detail', args=[group.pk])
        res = self.client.patch(url, data)
        self.assertContains(res, data['name'])

        # PATCH shouldn't be allowed if we are no longer a group admin
        group_user.is_group_admin = False
        group_user.save()
        res = self.client.patch(url, data)
        self.assertEqual(res.status_code, 403)

        # GET should still work without being admin
        res = self.client.get(url)
        self.assertEqual(res.status_code, 200)

    def test_group_users(self):
        user = User.objects.filter(is_staff=False).first()
        self.client.force_authenticate(user=user)
        user2 = User.objects.filter(is_staff=False).exclude(pk=user.pk).first()
        group_with_admin = mommy.make(
            Group,
        )
        fake_ciphertext = "x" * 500
        group_with_admin_user = mommy.make(
            GroupUser,
            group=group_with_admin,
            user=user,
            is_group_admin=True,
            private_key_ciphertext=fake_ciphertext,
            key_ciphertext=fake_ciphertext
        )
        group_no_admin = mommy.make(
            Group,
        )
        group_no_admin_user = mommy.make(
            GroupUser,
            group=group_no_admin,
            user=user,
            is_group_admin=False,
            private_key_ciphertext=fake_ciphertext,
            key_ciphertext=fake_ciphertext
        )
        group_no_users = mommy.make(Group)

        # Not part of this group, no access
        url = reverse('group-users-list', args=[group_no_users.pk])
        res = self.client.get(url)
        self.assertEqual(res.status_code, 403)

        # Group with membership
        url = reverse('group-users-list', args=[group_no_admin.pk])
        res = self.client.get(url)
        self.assertContains(res, user.id)
        self.assertContains(res, group_no_admin.id)

        # Get detail
        url = reverse('group-users-detail',
                      args=[group_no_admin.pk, group_no_admin_user.pk])
        res = self.client.get(url)
        self.assertContains(res, user.id)
        self.assertContains(res, group_no_admin.id)

        # Cannot add user to group when not group admin
        data = {
            "user": user2.pk,
            "is_group_admin": False,
            "key_ciphertext":  fake_ciphertext,
            "private_key_ciphertext": fake_ciphertext,
        }
        url = reverse('group-users-list', args=[group_no_admin.pk])
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 403)

        # Can add user to group when group admin
        url = reverse('group-users-list', args=[group_with_admin.pk])
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 201)
        group_user_id = res.data['id']

        # Cannot add same user twice
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 400)

        # Can edit group membership admin status when group admin
        data = {
            "is_group_admin": True,
        }
        url = reverse('group-users-detail',
                      args=[group_with_admin.pk, group_user_id])
        res = self.client.patch(url, data)
        self.assertContains(res, user2.id)
        self.assertContains(res, 'true')

        # Cannot change the user field
        data = {
            "user": 99999
        }
        url = reverse('group-users-detail',
                      args=[group_with_admin.pk, group_user_id])
        res = self.client.patch(url, data)
        self.assertEqual(res.data['user'], user2.id)

        # Can delete membership
        self.client.delete(url)
        self.assertFalse(
            group_with_admin.groupuser_set.filter(user=user2).exists()
        )
