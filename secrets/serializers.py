from django.core.exceptions import SuspiciousOperation
from rest_framework import serializers
from .models import Secret, SecretThrough
from access.models import Group


class SecretThroughSerializer(serializers.ModelSerializer):
    group = serializers.PrimaryKeyRelatedField(
        required=False,
        allow_null=True,
        default=None,
        queryset=Group.objects.all(),
    )
    public_key = serializers.SerializerMethodField()
    is_mine = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'request' in self.context:
            user = self.context['view'].request.user
            self.user_id = user.id
            self.my_groups = user.groups.values_list('id', flat=True)

    class Meta:
        model = SecretThrough
        fields = (
            'id', 'group', 'key_ciphertext', 'data', 'public_key',
            'is_mine'
        )

    def get_is_mine(self, obj):
        if hasattr(self, 'user_id'):
            if obj.group_id in self.my_groups or obj.user_id == self.user_id:
                return True
            return False

    def get_public_key(self, obj):
        if obj.group:
            return obj.group.public_key
        elif obj.user:
            return obj.user.public_key

    def update(self, instance, validated_data):
        secret_data = validated_data.pop('secret')
        secret = instance.secret

        secret.name = secret_data.get(
            'name',
            secret.name
        )
        secret.type = secret_data.get(
            'type',
            secret.type
        )
        secret.visible_data = secret_data.get(
            'data',
            secret.visible_data
        )
        secret.save()

        if (
            validated_data.get('owner') and
            instance.owner != validated_data['owner']
        ):
            raise SuspiciousOperation('Secrets api cannot change ownership')

        hidden_data = validated_data.get('data')
        if hidden_data is None or hidden_data == instance.hidden_data:
            # Nothing else to update
            return instance

        # We actually have to update EVERY secret's private data now
        for secret in secret.secret_through_set.all():
            # Only update hidden data. Other fields can't be changed.
            secret.set_hidden_data(hidden_data)
            secret.save()

            # We want to return the updated instance
            if secret.id == instance.id:
                my_secret = secret

        return my_secret


class SecretSerializer(serializers.ModelSerializer):
    secret_through_set = SecretThroughSerializer(many=True)

    class Meta:
        model = Secret
        fields = ('id', 'name', 'type', 'data', 'secret_through_set')

    def create(self, validated_data):
        secret_through_set_data = validated_data.pop('secret_through_set')
        secret = Secret.objects.create(**validated_data)
        for secret_through_data in secret_through_set_data:
            if secret_through_data.get('group'):
                user = None
            else:
                user = self.context['request'].user
            SecretThrough.objects.create(
                secret=secret,
                user=user,
                **secret_through_data)
        return secret

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.type = validated_data.get('type', instance.type)
        instance.visible_data = validated_data.get('data', instance.data)

        if self.partial:
            # Can't just use keys() because it contains secrets and data
            # which may not be part of the partial update
            fields = []
            if validated_data.get('name'):
                fields.append('name')
            if validated_data.get('type'):
                fields.append('type')
            if validated_data.get('data'):
                fields.append('data')
            instance.save(update_fields=fields)
            return instance

        secrets_data = validated_data.pop('secret_through_set')
        for secret_through in instance.secret_through_set.all():
            for secret_data in secrets_data:
                if (
                    secret_through.group == secret_data.get('group') or
                    secret_through.user == secret_data.get('user')
                ):
                    secret_through.key_ciphertext = secret_data.get(
                        'key_ciphertext', secret_through.key_ciphertext)
                    secret_through.data = secret_data.get(
                        'data', secret_through.data)
                    secret_through.save()
                # TODO handle new data here

        instance.save()
        return instance
