from django.conf import settings
from rest_framework import serializers
from .models import User, Group, GroupUser


class UserPublicAuthSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('client_alg', 'client_iterations', 'client_salt')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email', 'password', 'public_key',
                  'private_key', 'client_alg', 'client_iterations',
                  'client_salt')
        read_only_fields = ('id',)
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class PublicUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'public_key',)


class UserPublicKeySerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'public_key',)


class UsernameAvailableSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=254)


class GroupSerializer(serializers.ModelSerializer):
    key_ciphertext = serializers.CharField(write_only=True)
    private_key_ciphertext = serializers.CharField(write_only=True)

    class Meta:
        model = Group
        fields = ('id', 'name', 'slug', 'public_key', 'key_ciphertext',
                  'private_key_ciphertext')
        extra_kwargs = {
            'public_key': {'write_only': True},
        }

    def create(self, validated_data):
        user = self.context['request'].user
        return Group.objects.create(user, **validated_data)


class GroupUserSerializer(serializers.ModelSerializer):
    group = serializers.PrimaryKeyRelatedField(
        required=False, allow_null=True, read_only=True, default=None)

    class Meta:
        model = GroupUser
        fields = ('id', 'user', 'group', 'is_group_admin',
                  'key_ciphertext', 'private_key_ciphertext')
        read_only_fields = ('group',)
        extra_kwargs = {
            'key_ciphertext': {'write_only': True},
            'private_key_ciphertext': {'write_only': True},
        }

    def validate(self, data):
        if GroupUser.objects.filter(
            user=data.get('user'),
            group=self.context['view'].get_group()
        ).exists():
            raise serializers.ValidationError(
                "A user can only be added to a group once. ")
        return super().validate(data)


class GroupUserUpdateSerializer(serializers.ModelSerializer):
    """ No need to change the group or user when updating a through table """
    class Meta:
        model = GroupUser
        fields = ('id', 'user', 'group', 'is_group_admin',)
        read_only_fields = ('user', 'group',)


class DetailedGroupSerializer(serializers.ModelSerializer):
    groupuser_set = GroupUserSerializer(many=True)
    my_key_ciphertext = serializers.SerializerMethodField()
    my_private_key_ciphertext = serializers.SerializerMethodField()

    def get_my_key_ciphertext(self, obj):
        user = self.context['request'].user
        return obj.get_my_key_ciphertext(user)

    def get_my_private_key_ciphertext(self, obj):
        user = self.context['request'].user
        return obj.get_my_private_key_ciphertext(user)

    class Meta:
        model = Group
        fields = ('id', 'name', 'groupuser_set', 'public_key',
                  'my_key_ciphertext', 'my_private_key_ciphertext')
